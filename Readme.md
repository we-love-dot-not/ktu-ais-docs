# Individualus Planas
```
curl 'https://uais.cr.ktu.lt/ktuis/vs.ind_planas' -H 'Cookie: STUDCOOKIE=95D044E5A55E16BBBC44DB1C0A96E134F42FECA63B2D4E8F;'
```
### Request
```GET``` https://uais.cr.ktu.lt/ktuis/vs.ind_planas

### COOKIES
    STUDCOOKIE

### REGEX
```regex
/(<li><a href="STUD_SS2.planas_busenos).+(<\/a><\/li>)/g
```

### RESULT
```html
<li><a href="STUD_SS2.planas_busenos?plano_metai=2017&p_stud_id=583742">2017/2018m.m. Programų sistemos </a></li><li><a href="STUD_SS2.planas_busenos?plano_metai=2016&p_stud_id=583742">2016/2017 m. m. Programų sistemos </a></li><li><a href="STUD_SS2.planas_busenos?plano_metai=2015&p_stud_id=569975">2015/2016 m. m. Programų sistemos </a></li>
```

# Moduliai
---
```
curl 'https://uais.cr.ktu.lt/ktuis/STUD_SS2.planas_busenos?plano_metai=2017&p_stud_id=583742' -H 'Cookie: STUDCOOKIE=37A2D9455E2A4C33301FEBB9A713FCDB142888E1BAA4640E;'
```

### Request
```GET``` https://uais.cr.ktu.lt/ktuis/STUD_SS2.planas_busenos?plano_metai=2017&p_stud_id=583742

### PARAMS

plano_metai=2017

p_stud_id=583742

### COOKIES
    STUDCOOKIE

### REGEX
```regex
/(<td><a href="https:\/\/uais.cr.kt).+(target="_blank">)/g
```

### RESULT
```html
<td><a href="https://uais.cr.ktu.lt/ktuis/stp_report_ects.mdl_ml?p_kodas=P170B114&p_year=2017&p_lang=LT&p_stp_id=5289" target="_blank">
<td><a href="https://uais.cr.ktu.lt/ktuis/stp_report_ects.mdl_ml?p_kodas=T120B145&p_year=2017&p_lang=LT&p_stp_id=5289" target="_blank">
<td><a href="https://uais.cr.ktu.lt/ktuis/stp_report_ects.mdl_ml?p_kodas=P170B328&p_year=2017&p_lang=LT&p_stp_id=5289" target="_blank">
<td><a href="https://uais.cr.ktu.lt/ktuis/stp_report_ects.mdl_ml?p_kodas=P170B115&p_year=2017&p_lang=LT&p_stp_id=5289" target="_blank">
<td><a href="https://uais.cr.ktu.lt/ktuis/stp_report_ects.mdl_ml?p_kodas=P175B124&p_year=2017&p_lang=LT&p_stp_id=5289" target="_blank">
<td><a href="https://uais.cr.ktu.lt/ktuis/stp_report_ects.mdl_ml?p_kodas=T120B029&p_year=2017&p_lang=LT&p_stp_id=5289" target="_blank">
```

# Pažymiai
---
```
curl 'https://uais.cr.ktu.lt/ktuis/STUD_SS2.infivert' -H 'Cookie: STUDCOOKIE=902025233681CD420C32D01E0247A5F4614C6525207C736E;' --data 'p2=2016P6140721&p1=583740'
```

### REQUEST 
```POST``` 'https://uais.cr.ktu.lt/ktuis/STUD_SS2.infivert'

### PARAMS 
p2=2016P6140721 //p_year + p_kodas
p1=583740 // p_stp_id

### COOKIES 
STUDCOOKIE

### REGEX
```
(<table cellspacing=0 cellpadding=0 style="border-collapse:collapse; empty-cells:hide;" class="d_grd)[\s\S]+(<\/table>)(?=\n<table)
```

### RESULT
```html
<table cellspacing=0 cellpadding=0 style="border-collapse:collapse; empty-cells:hide;" class="d_grd2">
<COL style="width:25px;"><COL style="width:90px;"><COL style="width:150px;"><COL style="width:55px;">
<COL style="width:40px;"><COL style="width:22px;"><COL style="width:22px;"><COL style="width:22px;"><COL style="width:22px;"><COL style="width:22px;"><COL style="width:22px;"><COL style="width:22px;"><COL style="width:22px;"><COL style="width:22px;"><COL style="width:22px;"><COL style="width:22px;"><COL style="width:22px;"><COL style="width:22px;"><COL style="width:22px;"><COL style="width:40px;"><COL style="width:40px;"><COL style="width:35px;"><COL style="width:35px;"><COL style="width:70px;">
  <TR class="ttl" style="text-align:center;">
      <TD rowspan=2>Nr.</TD>
      <TD rowspan=2>Grup�</TD>
      <TD rowspan=2>Pavard�, vardas</TD> 
      <TD>Savait�</TD>
<td>1-4</td>
<td>5</td>
<td>6</td>
<td>7</td>
<td>8</td>
<td>9</td>
<td>10</td>
<td>11</td>
<td>12</td>
<td>13</td>
<td>14</td>
<td>15</td>
<td>16</td>
<td>16</td>
<td>16</td>
<td>17-20</td>
<TD rowspan=2>Si�lo-<BR>mas</TD>
      <td colspan=3>Galutinis �vert.</td>
  </tr>
  <TR class="ttl" style="text-align:center;">
      <td>U�duotis</td>
<TD class="grd_empt">&nbsp;</TD><TD>KL</TD><TD class="grd_empt">&nbsp;</TD><TD class="grd_empt">&nbsp;</TD><TD>LA</TD><TD class="grd_empt">&nbsp;</TD><TD>AT</TD><TD>KL</TD><TD>LA</TD><TD class="grd_empt">&nbsp;</TD><TD class="grd_empt">&nbsp;</TD><TD>LA</TD><TD>KL</TD><TD>K�</TD><TD>AT</TD><TD>E1</TD><td>�sk.</td><td>Pa�.</td><td>Data</td>
  </TR>
<TR class="dtr">
        <TD>68</TD>
        <TD>IFF-5/3</TD><TD>Povilaitis K�stutis</TD><TD>B6168</TD><TD class="grd_empt">&nbsp;</TD><TD class="grd">05</TD>
<TD class="grd_empt">&nbsp;</TD><TD class="grd_empt">&nbsp;</TD><TD class="grd"></TD>
<TD class="grd_empt">&nbsp;</TD><TD class="grd"></TD>
<TD class="grd"></TD>
<TD class="grd"></TD>
<TD class="grd_empt">&nbsp;</TD><TD class="grd_empt">&nbsp;</TD><TD class="grd"></TD>
<TD class="grd"></TD
<TD class="grd"></TD>
<TD class="grd"></TD>
<TD class="grd"></TD>
<TD class="grd">0,75</TD>
<TD class="grd"></TD><TD class="grd"></TD><TD class="grd"></TD>
</tr>
</table>
```

# Tvarkaraštis 
---
```
url 'https://uais.cr.ktu.lt/ktuis/tv_rprt2.ical1?p=B6168&t=basic.ics'
```
### Request
```GET``` https://uais.cr.ktu.lt/ktuis/tv_rprt2.ical1?p=B6168&t=basic.ics

### RESULT
```
tv_rprt2.ical1 tvarkaraščio failas
```
